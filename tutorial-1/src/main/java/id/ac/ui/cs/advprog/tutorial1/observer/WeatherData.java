package id.ac.ui.cs.advprog.tutorial1.observer;

import java.util.ArrayList;
import java.util.Observable;

public class WeatherData extends Observable {

    private float temperature;
    private float humidity;
    private float pressure;

    public WeatherData(){}

    public void measurementsChanged() {
        setChanged();
        notifyObservers();
    }

    public void setMeasurements(float temperature, float humidity,
                                float pressure) {
        this.temperature = temperature;
        this.humidity = humidity;
        this.pressure = pressure;
        measurementsChanged();
    }

    public float getTemperature() {
        // TODO Complete me!
        return temperature;
    }

    public void setTemperature(float tmp) {
        // TODO Complete me!
        temperature = tmp;
    }

    public float getHumidity() {
        // TODO Complete me!
        return humidity;
    }

    public void setHumidity(float hmd) {
        // TODO Complete me!
        humidity = hmd;
    }

    public float getPressure() {
        // TODO Complete me!
        return pressure;
    }

    public void setPressure(float press) {
        // TODO Complete me!
        pressure = press;
    }
}
